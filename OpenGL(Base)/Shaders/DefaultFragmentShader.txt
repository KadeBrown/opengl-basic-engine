#version 410
in vec4 vNormal;
in vec2 vTexCoord;
in vec4 vColor;

out vec4 fragColor;

uniform sampler2D diffuse;

void main()
{
	fragColor = texture(diffuse, vTexCoord);
	fragColor.a = 1;
}