#pragma once
#include <string>

class Program
{
public:
	Program()	{}
	~Program()	{}

	void CreateNewProgram(const char* vShaderLocation, const char* fShaderLocation);

	//Returns a string containing all contents of given text file (keeps file formatting)
	std::string LoadTextFromFile(const char* fileLocation);

	unsigned int GetProgramID() { return m_ProgramID; }

private:

	unsigned int m_ProgramID;
};

