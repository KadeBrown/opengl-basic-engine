#pragma once
#include "gl_core_4_4.h"
#include "glm\glm.hpp"
#include "glm\ext.hpp"
#include "glfw\include\GLFW\glfw3.h"

class Program;
class Texture;

using glm::vec2;
using glm::vec3;
using glm::vec4;
using glm::mat4;

class Grid
{
	struct Vertex
	{
		vec4 v_Position;
		vec4 v_Color;
		vec4 v_Normal;
		vec2 v_TexCoord;
	};

public:
	Grid();
	~Grid();

	void GenerateGrid(unsigned int rows, unsigned int cols, const char* vertShaderPath,
									const char* fragShaderPath, const char* texturePath);
	void Render();
	void GeneratePerlinData();

	void SetSeed(float newSeed)					{ m_Seed = newSeed;}
	void SetScale(float newScale)				{ m_Scale = newScale;}
	void SetOctaves(float newOctaves)			{ m_Octaves = newOctaves;}
	void SetAmplitude(float newAmplitude)		{ m_Amplitude = newAmplitude;}
	void SetFrequency(float newFrequency)		{ m_Frequency = newFrequency;}
	void SetPersistance(float newPersistance)	{ m_Persistance = newPersistance;}

private:
	int m_Dims;
	int m_TotalRows;
	int m_TotalCols;

	float m_Seed;
	float m_Scale;
	float m_Octaves;
	float m_Amplitude;
	float m_Frequency;
	float m_Persistance;
	float *m_PerlinData;

	unsigned int m_VAO;
	unsigned int m_VBO;
	unsigned int m_IBO;
	unsigned int m_Rows;
	unsigned int m_Cols;
	unsigned int m_IndiceCount;
	unsigned int m_PerlinTexture;

	Program* m_Program;
	Texture* m_Texture;

};

