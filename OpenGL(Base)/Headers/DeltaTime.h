#pragma once

#define TIME (Time::_Instance())
#define DeltaTime (Time::_Instance()).GetDeltaTime();

class Time
{
public:
	static Time& _Instance();
	
	float GetDeltaTime();
	void UpdateDeltaTime();
	void SetTimeScale(float scale);


private:

	Time() { m_PreviousFrame = 0; }
	Time(const Time&) = delete;
	void operator = (const Time&) = delete;

	float  m_DeltaTime;
	float  m_TimeScale = 1;
	float  m_PreviousFrame;
};

