#pragma once
#include "Camera.h"


class FlyCamera : public Camera
{
public:
	FlyCamera() {}
	~FlyCamera(){}

	virtual void Update();
	void GetInput();
	void GetFrustumPlanes(const mat4& transform, vec4* planes);
	inline void SetSpeed(float speed) { m_Speed = speed; }
	inline void SetSensitivity(float sensitivity) { m_Sensitivity = sensitivity; }

	vec4 m_FrustumPlanes[6];

private:
	void CalculateRotation(double xOffset, double yOffset);

	bool  m_MouseClicked;
	float m_CursorX;
	float m_CursorY;
	float m_InitSpeed       = 2.0f;
	float m_Speed			= 0;
	float m_SpeedMultiplier = 2;
	float m_Sensitivity		= 0.002f;
	vec3 m_Up;

};

