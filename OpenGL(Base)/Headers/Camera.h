#pragma once
#include "glm\glm.hpp"
#include "glm\ext.hpp"
#include "glfw\include\GLFW\glfw3.h"

using glm::vec3;
using glm::vec4;
using glm::mat4;

class Camera
{
public:
	Camera()  {}
	~Camera() {}

	virtual void Update();
	void SetPerspective(float fieldOfView, float aspectRatio, float near, float far);
	void LookAt(vec3 position, vec3 up);
	void SetPosition(vec3 newPosition);
	void SetTransform(mat4 newTransform);

	inline mat4 GetViewTransform()	{ return m_ViewTransform; }
	inline mat4 GetWorldTransform() { return m_WorldTransform; }
	inline mat4 GetProjection()		{ return m_ProjectionTransform; }
	inline mat4 GetProjectionView() { return m_ProjectionViewTransform; }

	mat4 m_View;
	mat4 m_Projection;
	vec3 m_Position;

protected:
	vec3 m_Direction;
	vec3 m_Target;
	vec3 m_CameraUp;
	vec3 m_CameraRight;
	vec3 m_CameraForward;
	vec3 m_WorldUp;

private:
	void UpdateProjectionViewTransform();

	mat4 m_WorldTransform;
	mat4 m_ViewTransform;
	mat4 m_ProjectionTransform;
	mat4 m_ProjectionViewTransform;

};

