#pragma once
#include <list>
#include <gl_core_4_4.h>
#include <glfw\include\GLFW\glfw3.h>
#include "glm\glm.hpp"
#include "glm\ext.hpp"
#include "GameObject.h"
#include "FlyCamera.h"

#define APPLICATION (Application::_Instance())


typedef std::list<GameObject*>::iterator objIter;

using glm::vec3;
using glm::vec4;
using glm::mat4;

class Grid;

class Application
{
public:
	static Application& _Instance();


	//Sets up the application for the first time
	//will return false if any part of the initialisation process fails 
	bool Initialise();

	//Main while loop 
	void Run();

	//Updates all objects
	void Update();

	//Renders all objects
	void Render();

	//Clears Necessary memory
	void Shutdown();

	//Use this to stop the game
	void ChangeRunState(bool runState) { m_Run = runState; }

	//Adds Game object to list of object to be updated
	void AddGameObject(GameObject* objToAdd);

	//Adds Game object to list to be deleted next frame
	void RemoveGameObject(GameObject* objToDelete);

	//Updates all Game objects in list
	void UpdateGameObjects();

	//Renders all Game objects in list
	void RenderGameObjects();

	//Deletes all objects within Objects to delete list
	void DeleteObjects();

	void InitialiseMainCamera();
	
	//Finds first active object with given name
	GameObject* FindGameObjectWithName(std::string name);

	//Finds first active object with given tag
	GameObject* FindGameObjectWithTag(std::string tag);

	//Finds all active objects with given name
	std::list<GameObject*> FindAllGameObjectsWithName(std::string name);

	//Finds all active objects with given tag
	std::list<GameObject*> FindAllGameObjectsWithTag(std::string tag);

	GLFWwindow* GetWindow()   { return m_Window; }
	FlyCamera* GetFlyCamera() { return m_MainCam; }

	std::list<GameObject*> m_GameObjects;
	std::list<GameObject*> m_ObjectsToDelete;

	vec4 m_WindowColor;

private:

	Application()	{}
	~Application()	{}
	Application(const Application&)		 = delete;
	void operator = (const Application&) = delete;

	bool m_Run = true;
	GLFWwindow* m_Window;
	FlyCamera* m_MainCam;
	Grid* m_Grid;

};

