#pragma once


class Texture
{
public:
	Texture()  {}
	~Texture() {}

	void LoadTexture(const char* textureLocation);

	unsigned int GetTexture()	{ return m_Texture; }
	int GetImageWidth()			{ return m_ImageWidth;  }
	int GetImageHeight()		{ return m_ImageHeight; }
	const char* GetTextureLocation() const { return m_TextureLocation; }
private:

	int m_ImageWidth;
	int m_ImageHeight;
	int m_ImageFormat;
	unsigned int m_VAO;
	unsigned int m_VBO;
	unsigned int m_IBO;
	unsigned int m_Texture;
	unsigned char* m_Data;
	const char* m_TextureLocation;

};

