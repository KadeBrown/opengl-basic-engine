#pragma once
#include <iostream>
#include <string>

#define Debug (MyDebug::_Instance())

class MyDebug
{
public:
	static MyDebug& _Instance();

	template<typename T>
	void Log(T msg)
	{
		std::cout << msg << std::endl;
	}

private:
	MyDebug()  {}
	~MyDebug() {}
	MyDebug(const MyDebug&)			   = delete;
	void operator = (const MyDebug&) = delete;
};

