#pragma once
#include <string>
#include "glm\glm.hpp"
#include "glm\ext.hpp"

class Program;

using glm::vec3;
using glm::mat4;

class GameObject
{
public:
	GameObject();
	GameObject(const char* vShaderLocation, const char* fShaderLocation);
	~GameObject();

	virtual void Update();
	virtual void Render();


	Program* GetProgram() { return m_Program; }


	void SetPosition(glm::vec3 newPosition) { m_Position = newPosition; }


	void SetName(std::string newName)	{ m_Name = newName; }
	void SetTag(std::string newTag)		{ m_Tag = newTag;   }

	std::string GetName() { return m_Name; }
	std::string GetTag()  { return m_Tag;  }

protected:
	mat4 m_LocalTransform;
	mat4 m_GlobalTransform;
	vec3 m_Position;

private:
	Program* m_Program;
	std::string m_Name	= "No Name";
	std::string m_Tag	= "No Tag";

};

