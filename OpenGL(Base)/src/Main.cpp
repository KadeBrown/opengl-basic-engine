#include "Application.h"
#include <iostream>


int main(int, char**)
{
	printf("This project was created by Kade Brown. \n");
	if (APPLICATION.Initialise())
		APPLICATION.Run();
	return 0;
}

/*
TODO LIST OF THINGS TODO:
-Camera
-Keyboard Input
-Default Shaders
-FBX Loader
-Texture Class
MAYBE:
-Particle System
-Terrain
-Collision Detection
-GUI
-Fluid Effects
-Skybox
*/