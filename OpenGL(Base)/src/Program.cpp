#include "Program.h"
#include <gl_core_4_4.h>
#include <iostream>
#include <fstream>


void Program::CreateNewProgram(const char* vShaderLocation, const char* fShaderLocation)
{

	//Reads all text from text file at the given location
	const std::string vShaderString = LoadTextFromFile(vShaderLocation);
	const std::string fShaderString = LoadTextFromFile(fShaderLocation);

	const GLchar* vShader = vShaderString.c_str();
	const GLchar* fShader = fShaderString.c_str();

	int glSuccess = GL_FALSE;
	unsigned int vertexShader   = glCreateShader(GL_VERTEX_SHADER);
	unsigned int fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);

	//Creates vertex shader
	glShaderSource(vertexShader, 1, &vShader, 0);
	glCompileShader(vertexShader);

	//Creates fragment shader
	glShaderSource(fragmentShader, 1, &fShader, 0);
	glCompileShader(fragmentShader);

	//Create program and attach shaders
	m_ProgramID = glCreateProgram();
	glAttachShader(m_ProgramID, vertexShader);
	glAttachShader(m_ProgramID, fragmentShader);
	glLinkProgram(m_ProgramID);
	glGetProgramiv(m_ProgramID, GL_LINK_STATUS, &glSuccess);

	//Checks if program and shader have been created and linked properly
	if (glSuccess == GL_FALSE)
	{
		int infoLogLength = 0;
		glGetProgramiv(m_ProgramID, GL_INFO_LOG_LENGTH, &infoLogLength);

		char* infoLog = new char[infoLogLength];
		glGetProgramInfoLog(m_ProgramID, infoLogLength, 0, infoLog);

		printf("Error: Failed to link shader to program!\n");
		printf("%s\n", infoLog);

		delete[] infoLog;
	}

	//Clears necessary memory
	glDeleteShader(fragmentShader);
	glDeleteShader(vertexShader);



}

std::string Program::LoadTextFromFile(const char* fileLocation)
{
	std::ifstream file(fileLocation, std::ios::in);
	std::string line = "";
	std::string fileText;

	if (file.is_open())
	{
		while (!file.eof())
		{
			std::getline(file, line);
			fileText += line + "\n";
		}
		file.close();
	}

	return fileText;
}
