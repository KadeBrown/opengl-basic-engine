#include "Application.h"
#include "GameObject.h"
#include "Program.h"




GameObject::GameObject()
{
	APPLICATION.AddGameObject(this);
	m_Program = new Program();
}

GameObject::GameObject(const char * vShaderLocation, const char * fShaderLocation)
{
	APPLICATION.AddGameObject(this);
	m_Program = new Program();
	m_Program->CreateNewProgram(vShaderLocation, fShaderLocation);
}


GameObject::~GameObject()
{
	APPLICATION.RemoveGameObject(this);
}

void GameObject::Update()
{

}

void GameObject::Render()
{

}
