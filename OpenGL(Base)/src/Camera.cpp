#include "Camera.h"


void Camera::Update()
{
	LookAt(m_Position + m_CameraForward, vec3(0, 1, 0));
}

void Camera::SetPerspective(float fieldOfView, float aspectRatio, float near, float far)
{
	m_ProjectionTransform = glm::perspective(fieldOfView, aspectRatio, near, far);
	UpdateProjectionViewTransform();
}

void Camera::LookAt(vec3 position, vec3 up)
{
	m_WorldTransform = (glm::inverse(glm::lookAt(vec3(m_WorldTransform[3]), position, up)));
	UpdateProjectionViewTransform();
}

void Camera::SetPosition(vec3 newPosition)
{
	m_WorldTransform[3] = vec4(newPosition, 1);
	m_Position = vec3(m_WorldTransform[3].x, m_WorldTransform[3].y, m_WorldTransform[3].z);
	UpdateProjectionViewTransform();
}

void Camera::SetTransform(mat4 newTransform)
{
	m_WorldTransform = newTransform;
	UpdateProjectionViewTransform();
}

void Camera::UpdateProjectionViewTransform()
{
	m_ProjectionViewTransform = m_ProjectionTransform * glm::inverse(m_WorldTransform);
}
