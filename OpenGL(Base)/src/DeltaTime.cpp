#include "Application.h"
#include "DeltaTime.h"



Time & Time::_Instance()
{
	static Time time;
	return time;
}

float Time::GetDeltaTime()
{
	return m_DeltaTime;
}

void Time::UpdateDeltaTime()
{
	float currentFrame = (float)glfwGetTime();
	m_DeltaTime = (currentFrame - m_PreviousFrame) * m_TimeScale;
	m_PreviousFrame = currentFrame;
}

void Time::SetTimeScale(float scale)
{
	m_TimeScale = scale;
}



